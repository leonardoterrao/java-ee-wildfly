package com.leonardoterrao.exceptions;

public class NoSuchSeatException extends Exception {

    public NoSuchSeatException(String message) {
        super(message);
    }

}
