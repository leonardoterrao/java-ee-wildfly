package com.leonardoterrao.exceptions;

public class SeatBookedException extends Exception {

    public SeatBookedException(String message) {
        super(message);
    }

}
