package com.leonardoterrao.com.leonardoterrao.boundary;

import com.leonardoterrao.control.TheatreBox;
import com.leonardoterrao.entity.Seat;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Collection;

@Stateless
@Remote(TheatreInfo.class)
public class TheatreInfo implements TheatreInfoRemote {

    @EJB
    private TheatreBox theatreBox;

    @Override
    public String printSeatList() {
        final Collection<Seat> seats = theatreBox.getSeats();
        final StringBuilder sb = new StringBuilder();
        for(Seat seat : seats) {
            sb.append(seat.toString());
            sb.append(System.lineSeparator());
        }

        return sb.toString();
    }
}
