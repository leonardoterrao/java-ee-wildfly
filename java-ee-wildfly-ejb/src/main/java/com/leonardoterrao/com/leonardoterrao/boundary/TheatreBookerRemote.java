package com.leonardoterrao.com.leonardoterrao.boundary;

import com.leonardoterrao.exceptions.NoSuchSeatException;
import com.leonardoterrao.exceptions.NotEnoughMoneyException;
import com.leonardoterrao.exceptions.SeatBookedException;

import javax.ejb.Asynchronous;
import java.util.concurrent.Future;

public interface TheatreBookerRemote {

    int getAccountBalance();

    String bookSeat(int seatId) throws SeatBookedException, NotEnoughMoneyException, NoSuchSeatException;

}
